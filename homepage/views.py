from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import authenticate, login as login_auth, logout as logout_auth

# source :https://simpleisbetterthancomplex.com/tutorial/2017/02/18/how-to-create-user-sign-up-view.html


# Create your views here.
def index(request):
    user = request.user
    if user.is_authenticated:
        return render(request, 'welcome.html')        
    else:
        return render(request, 'index.html')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login_auth(request, user)
            return redirect('homepage:index')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def login(request):
    user = request.user
    login_form = AuthenticationForm()
    
    if user.is_authenticated:
        return redirect('homepage:index')

    if request.method == "POST":
        login_form = AuthenticationForm(data = request.POST)
        if login_form.is_valid():
            user = login_form.get_user()
            login_auth(request, user)
            return redirect('homepage:index')
    return render(request, 'loginpage.html', {'form':login_form})

def logout(request):
    if request.method == 'POST':
        logout_auth(request)
    return redirect('homepage:index')

