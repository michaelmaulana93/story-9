from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login, name="loginpage"),
    path('logout', views.logout, name="logout"),
    path('signup', views.signup, name="signup"),
]