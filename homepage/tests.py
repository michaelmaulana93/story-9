from django.test import TestCase, Client
from django.urls import resolve
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class TestIndex(TestCase):
    def test_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/signup')
        self.assertEqual(response.status_code, 200)

    def test_views_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_uses_form_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
        response = self.client.get('/login')
        self.assertTemplateUsed(response, 'loginpage.html')
        response = self.client.get('/signup')
        self.assertTemplateUsed(response, 'signup.html')

    def test_views(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)
        found = resolve('/login')
        self.assertEqual(found.func, views.login)
        found = resolve('/signup')
        self.assertEqual(found.func, views.signup)
        found = resolve('/logout')
        self.assertEqual(found.func, views.logout)



class TestFunctional(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestFunctional, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(TestFunctional, self).tearDown()

    def test_opening_page(self):
        self.selenium.get('http://127.0.0.1:8000/')
        self.assertEqual(self.selenium.title, "Story 9 Homepage")

    def test_opening_loginpage(self):
        self.selenium.get('http://127.0.0.1:8000/login')
        self.assertEqual(self.selenium.title, "Login")

    def test_opening_loginpage(self):
        self.selenium.get('http://127.0.0.1:8000/signup')
        self.assertEqual(self.selenium.title, "Login")

    def test_login(self):
        self.selenium.get('http://localhost:8000/login')
        username = self.selenium.find_element_by_id("id_username")
        username.send_keys("admin")
        password = self.selenium.find_element_by_id("id_password")
        password.send_keys("adminpassword")
        password.submit()
        self.assertIn("Welcome, admin", self.selenium.page_source)
